const withTM = require('next-transpile-modules')(
    ['@amcharts/amcharts4/core.js', '@amcharts/amcharts4/themes/animated.js',
    '@amcharts/amcharts4/plugins/sunburst.js', '@amcharts/amcharts4/charts.js']); // pass the modules you would like to see transpiled

module.exports = withTM();
