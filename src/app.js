import 'global-jsdom/register';
import JSDOMUtils from 'jsdom/lib/jsdom/living/generated/utils.js';
import JSDOMSVGSVGElementImpl from 'jsdom/lib/jsdom/living/nodes/SVGSVGElement-impl.js';
import * as SVGDOM from 'svgdom';

import * as am4core from "@amcharts/amcharts4/core.js";
import * as am4charts from "@amcharts/amcharts4/charts.js";
import fs from 'fs';

// Importing themes
import am4themes_dark from "@amcharts/amcharts4/themes/dark.js";
import * as am4sunburst from '@amcharts/amcharts4/plugins/sunburst.js';


const JSDOMcreateElementNS = document.createElementNS;
global.SVGPathElement = SVGDOM.SVGPathElement;
document.createElementNS = createElementNS;
document.createAttributeNS = createAttributeNS;


window.innerWidth = 500;
window.innerHeight = 500;

const container = document.createElement('div');
container.innerHeight = 500;
container.innerWidth = 500;
// pour éviter de laisser jsdom se planter en remontant trop haut
container.style.font = 'Arial';
container.style['font-family'] = 'serif';
container.style['font-size'] = '12px';
Object.defineProperties(container, {
    clientWidth: {
        value: 500
    },
    clientHeight: {
        value: 500
    },
    offsetWidth: {
        value: 500
    },
    offsetHeight: {
        value: 500
    },
});
document.body.append(container);

am4core.ready(() => {

    var amContainer = am4core.create(container, am4core.Container);
    amContainer.width = 1024;
    amContainer.height = 768;
    let chart = am4core.create(amContainer, am4sunburst.Sunburst);
    chart.width = am4core.percent(100);
    chart.height = am4core.percent(100);
    chart.radius = am4core.percent(100);
    amContainer.width = 1024;
    amContainer.height = 768;
    // Add multi-level data
    chart.data = [{
        name: "First",
        children: [
            { name: "A1", value: 100 },
            { name: "A2", value: 60 }
        ]
    },
        {
            name: "Second",
            children: [
                { name: "B1", value: 135 },
                { name: "B2", value: 98 }
            ]
        },
        {
            name: "Third",
            children: [
                {
                    name: "C1",
                    children: [
                        { name: "EE1", value: 130 },
                        { name: "EE2", value: 87 },
                        { name: "EE3", value: 55 }
                    ]
                },
                { name: "C2", value: 148 },
                {
                    name: "C3", children: [
                        { name: "CC1", value: 53 },
                        { name: "CC2", value: 30 }
                    ]
                },
                { name: "C4", value: 26 }
            ]
        },
        {
            name: "Fourth",
            children: [
                { name: "D1", value: 415 },
                { name: "D2", value: 148 },
                { name: "D3", value: 89 }
            ]
        },
        {
            name: "Fifth",
            children: [
                {
                    name: "E1",
                    children: [
                        { name: "EE1", value: 33 },
                        { name: "EE2", value: 40 },
                        { name: "EE3", value: 89 }
                    ]
                },
                {
                    name: "E2",
                    value: 148
                }
            ]
        }];

    // Define data fields
    chart.dataFields.value = "value";
    chart.dataFields.name = "name";
    chart.dataFields.children = "children";

    chart.events.on('ready', () => {
        console.log('ready');
        setTimeout(() => {

            chart.exporting.getImage("svg").then((data) => {
                fs.writeFile('sunburst.svg', data, () => {
                    console.log("Image exportée");
                });
            });
        }, 1000);
    });
});


/**
 * Polyfill (dummy ;) createElementNS
 */
/*
function createElementNS (ns, name) {
    function element (name) {
        this.name = name
    }

    function setAttributeNS(namespace, qualifiedName, value) {
        this[qualifiedName] = value
    }

    function setAttribute(name, value) {
        this[name] = value
    }

    element.prototype.setAttributeNS = setAttributeNS
    element.prototype.setAttribute = setAttribute

    function createSVGRect () {}

    element.prototype.createSVGRect = createSVGRect

    return document.createElement(name);
}
*/
function createElementNS(ns, name) {
    if (ns === 'http://www.w3.org/2000/svg') {
        let elementType;
        switch (name.toLowerCase()) {
            case 'svg':
                elementType = SVGDOM.SVGSVGElement;
                break;
            case 'path':
                elementType = SVGDOM.SVGPathElement;
                break;
            case 'text':
            case 'tspan':
            case 'tref':
            case 'altglyph':
            case 'textpath':
                elementType = SVGDOM.SVGTextContentElement;
                break;
            default:
                elementType = SVGDOM.SVGGraphicsElement;
                break;
        }
        /*
        function LocalElem() {
            new Element(name, {
                ownerDocument: document,
                local: false
            }, ns);
            Element.call(this, name, {
                ownerDocument: document,
                local: false
            }, ns);
        }
        LocalElem.prototype = Object.create(elementType.prototype);
        LocalElem.prototype.constructor = LocalElem;
        return new LocalElem();
        */

        const elem = new elementType(name, {
            ownerDocument: document,
            local: false
        }, ns);
        const jsDOMElem = JSDOMcreateElementNS.call(document, ns, name);
        /*
        Object.defineProperty(elem, JSDOMUtils.implSymbol, {
            value: Object.create(JSDOMSVGSVGElementImpl.implementation.prototype),
            configurable: true
        });
        if (JSDOMSVGSVGElementImpl.init) {
            JSDOMSVGSVGElementImpl.init(elem[implSymbol]);
        }
        */
        elem[JSDOMUtils.implSymbol] = jsDOMElem[JSDOMUtils.implSymbol];
        if (name === 'svg') {
            elem.parentNode = container;
        }
        return elem;
    } else {
        return document.createElement(name);
    }
}

function createAttributeNS(namespace, qualifiedName) {
    return new SVGDOM.Attr(qualifiedName, { ownerDocument: document, local: false }, namespace);
}
