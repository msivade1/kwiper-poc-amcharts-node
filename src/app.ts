import 'global-jsdom/register';
import * as am4core from "@amcharts/amcharts4/core.js";
import * as am4charts from "@amcharts/amcharts4/charts.js";

// Importing themes
import am4themes_animated from "@amcharts/amcharts4/themes/animated.js";
import am4themes_dark from "@amcharts/amcharts4/themes/dark.js";
import * as am4sunburst from '@amcharts/amcharts4/plugins/sunburst.js';

am4core.useTheme(am4themes_animated);

window.SVGPathElement = () => {};
const container = document.createElement('div');
document.body.append(container);

let chart = am4core.create(container, am4sunburst.Sunburst);

chart.radius = am4core.percent(100);

// Add multi-level data
chart.data = [{
    name: "First",
    children: [
        { name: "A1", value: 100 },
        { name: "A2", value: 60 }
    ]
},
    {
        name: "Second",
        children: [
            { name: "B1", value: 135 },
            { name: "B2", value: 98 }
        ]
    },
    {
        name: "Third",
        children: [
            {
                name: "C1",
                children: [
                    { name: "EE1", value: 130 },
                    { name: "EE2", value: 87 },
                    { name: "EE3", value: 55 }
                ]
            },
            { name: "C2", value: 148 },
            {
                name: "C3", children: [
                    { name: "CC1", value: 53 },
                    { name: "CC2", value: 30 }
                ]
            },
            { name: "C4", value: 26 }
        ]
    },
    {
        name: "Fourth",
        children: [
            { name: "D1", value: 415 },
            { name: "D2", value: 148 },
            { name: "D3", value: 89 }
        ]
    },
    {
        name: "Fifth",
        children: [
            {
                name: "E1",
                children: [
                    { name: "EE1", value: 33 },
                    { name: "EE2", value: 40 },
                    { name: "EE3", value: 89 }
                ]
            },
            {
                name: "E2",
                value: 148
            }
        ]
    }];

// Define data fields
chart.dataFields.value = "value";
chart.dataFields.name = "name";
chart.dataFields.children = "children";

chart.exporting.export("png");
